# Frontend Mentor - Blog preview card solution

This is a solution to the [Blog preview card challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/blog-preview-card-ckPaj01IcS). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to:

- See hover and focus states for all interactive elements on the page

### Screenshot

![](./screenshot.png)

### Links

- Solution URL: [https://gitlab.com/my-frontend-mentor-repos/blog-preview-card](https://gitlab.com/my-frontend-mentor-repos/blog-preview-card)
- Live Site URL: [https://blog-preview-card-my-frontend-mentor-repos-0a9f7985d7f1562332db.gitlab.io/](https://blog-preview-card-my-frontend-mentor-repos-0a9f7985d7f1562332db.gitlab.io/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox

### What I learned

I used this exercise to practice some stuff and initiate myself on Frontend Mentor.

### Continued development

I want to focus and improve my skills and knowledge on CSS in order to be able to create better User Interfaces.

## Author

- Frontend Mentor - [@Dartheryon](https://www.frontendmentor.io/profile/Dartheryon)


## Acknowledgments

I want to thank my family for all the support they have provided on these hard times. My wife Diana, for being always here giving me the strength and faith i've been losing. My parents Jorge and Cristina, for their help, confidence and support.

My dear sister Diana for being here encouraging me to push harder and keep fighting.
My nephew Camilo for being the one I want to inspire.
